import java.io.*;

class program8{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size=Integer.parseInt(br.readLine());
		byte arr[]=new byte[size];

		for(int i=0;i<size;i++){
		
			arr[i]=Byte.parsebyte(br.readLine());
		}

		System.out.println("Output:");
		

		for(int j=0;j<size;j++){
		
			if(arr[j]<10){
			
				System.out.println(arr[j]);
			}
		}
		

	}
}
