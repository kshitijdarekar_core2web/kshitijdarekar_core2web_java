import java.io.*;

class program10{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter employee size : ");
		int size=Integer.parseInt(br.readLine());
		int empID[]=new int[size];

		System.out.println("Enter employee ID's");

		for(int i=0;i<size;i++){
		
			empID[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Output:");
		

		for(int j=0;j<size;j++){
		
			System.out.println(empID[j]);
	
		}
		

	}
}
