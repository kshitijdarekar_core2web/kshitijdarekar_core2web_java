
import java.io.*;

class program6{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size=Integer.parseInt(br.readLine());
		char arr[]=new char[size];

		for(int i=0;i<size;i++){
		
			arr[i]=(char)br.read();

			if (arr[i] == '\n') {

                		i--;

            		}

		}

		System.out.println("Output:");

		for(int j=0;j<size;j++){
		
			System.out.println(arr[j]);
		}
		

	}
}
