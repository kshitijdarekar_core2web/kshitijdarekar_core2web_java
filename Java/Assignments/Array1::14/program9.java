import java.io.*;

class program9{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		for(int i=0;i<size;i++){
		
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Output:");
		

		for(int j=0;j<size;j++){
		
			if(j%2 != 0){
			
				System.out.println(arr[j]+" is odd indexed element");
			}
		}
		

	}
}
