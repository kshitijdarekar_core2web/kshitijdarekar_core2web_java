


import java.io.*;

class program6{
	public static void main(String[] args)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.print("Enter number of rows : " );
	int row=Integer.parseInt(br.readLine());
 	

	for(int i=1;i<=row;i++){
	
		int temp=1;
        	int ch=97;	
		for(int j=1;j<=row-i+1;j++){
			
			if(j%2==0){
				System.out.print((char)ch+" ");
				ch++;
			}else{
			
				System.out.print(temp+" ");
				temp++;
			}


		}
		System.out.println();
	}
   }
}
