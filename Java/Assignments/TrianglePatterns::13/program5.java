


import java.io.*;

class program5{
	public static void main(String[] args)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.print("Enter number of rows : " );
	int row=Integer.parseInt(br.readLine());
 	
	int Salphabets=97;
	int Balphabets=65;

	for(int i=1;i<=row;i++){
		
		for(int j=1;j<=row-i+1;j++){
		
			if(i%2==0){
			
				System.out.print((char)Salphabets+" ");
				Salphabets++;
			}else{
				System.out.print((char)Balphabets+" ");
				Balphabets++;
			}		
	


		}
		System.out.println();
	}
   }
}
