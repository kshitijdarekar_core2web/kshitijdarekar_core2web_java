


import java.io.*;

class program7{
	public static void main(String[] args)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.print("Enter number of rows : " );
	int row=Integer.parseInt(br.readLine());
 	
	int num=row;

	for(int i=1;i<=row;i++){
		
		int no=96+num;

		int k=row-i+1;

		char ch=(char)no;

		for(int j=1;j<=row-i+1;j++){
		
			if(j%2!=0){
			
				System.out.print(k+"   ");
			}else{
			
				System.out.print(ch+"  ");
			}
			k--;
			ch--;
		
		}
		num--;
		System.out.println();
	}
   }
}
