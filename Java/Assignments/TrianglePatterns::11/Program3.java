import java.io.*;

class program3{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int row=Integer.parseInt(br.readLine());

		
		for(int i=1;i<=row;i++){
			
			int temp=row;

			for(int j=1;j<=i;j++){
			
				System.out.print(temp+" ");
				temp--;
			}
			System.out.println();
		}


	}
}
