import java.util.*;

class program4{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int row=sc.nextInt();

		int ch=64+row;

		int temp=row;

		for(int i=0;i<row;i++){
		
			for(int j=0;j<row;j++){
			
				if(temp%row==0){
				
					System.out.print((char)ch+"\t");
				
				}else{
				
					System.out.print(temp+"\t");
				}
				ch++;
				temp++;
			}
			System.out.println();
		}
	}
}
