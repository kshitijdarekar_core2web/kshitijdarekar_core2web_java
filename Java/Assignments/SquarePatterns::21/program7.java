import java.util.*;

class program7{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int row=sc.nextInt();

		int temp=row;

		for(int i=1;i<=row;i++){

			int ch=64+i;
		
			for(int j=1;j<=row;j++){
			
				
				if(temp%2==0){
				
					System.out.print(temp+"\t");
				}else{	

					System.out.print((char)ch+"\t");

				}
				temp++;


			}

			System.out.println();
		}
	}
}
