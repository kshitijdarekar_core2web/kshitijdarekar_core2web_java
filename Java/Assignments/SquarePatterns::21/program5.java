import java.util.*;

class program5{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int row=sc.nextInt();

		int temp=row;

		for(int i=0;i<row;i++){
		
			for(int j=0;j<row;j++){
			
				if(temp%2==0){
				
					System.out.print(temp*temp+"\t");
				
				}else{
				
					System.out.print(temp+"\t");
				}
				temp++;
			}
			System.out.println();
		}
	}
}
