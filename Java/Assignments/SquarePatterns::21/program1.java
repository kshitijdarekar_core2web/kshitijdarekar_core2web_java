import java.util.*;

class program1{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int rows=sc.nextInt();

		int temp=rows;

		System.out.println("Pattern:");
		
		for(int i=0;i<rows;i++){
			
			int ch=64+rows;

			for(int j=0;j<rows;j++){
			
				if(i%2==0){

					System.out.print((char)ch);
					ch--;
				}else{
				
					System.out.print(temp);
				}
				
			}
			System.out.println();
		}
	}
}
