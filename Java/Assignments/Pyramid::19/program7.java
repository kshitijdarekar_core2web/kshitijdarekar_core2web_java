import java.util.*;

class program7{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
		
			int ch=64+i;

			for(int j=1;j<=row-i;j++){
			
				System.out.print(" ");
			}

			for(int j=1 ; j<=2*i-1 ; j++){
			
				if(i%2==0){
				
					System.out.print((char)ch);
				}else{
				
					System.out.print(i);
				}
			}
			System.out.println();
		}
	}

}
