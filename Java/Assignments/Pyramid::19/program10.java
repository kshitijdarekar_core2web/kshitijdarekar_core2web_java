import java.io.*;

class program10{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows :");
		int row=Integer.parseInt(br.readLine());

		for(int i=1 ; i<=row ; i++){
			
			int ch=65;

			for(int j=1 ; j<=row-i ; j++){
			
				System.out.print(" ");
				ch++;
			}

			for(int j=1 ; j<=2*i-1 ; j++){
				
				if(j<i){
					
					System.out.print((char)ch);
					ch++;
					
				}else{
				
					System.out.print((char)ch);
					ch--;
					
				}
				
			}
			System.out.println();
		}
	}
}
