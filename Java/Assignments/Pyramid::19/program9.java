import java.util.*;

class program9{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();

		

		for(int i=1;i<=row;i++){
		
			int chS=97;
			int chC=65;

			for(int j=1;j<=row-i;j++){
			
				System.out.print(" ");
			}

			for(int j=1 ; j<=2*i-1 ; j++){
			
				if(i%2==0){
					
					if(j<i){
						System.out.print((char)chS);
						chS++;
					}else{
					
						System.out.print((char)chS);
						chS--;
					}
				}else{
				
					 if(j<i){
                                                System.out.print((char)chC);
                                                chC++;
                                        }else{

                                                System.out.print((char)chC);
                                                chC--;
                                        }
				}
			}
			System.out.println();
		}
	}

}
