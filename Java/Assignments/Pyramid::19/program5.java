
import java.io.*;

class program5{

	public static void main(String[] args)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.print("Enter number of rows: ");
	int rows=Integer.parseInt(br.readLine());
    
	System.out.println("Output :");
	

	for(int i=1;i<=rows;i++){

		int ch=1;
	
		for(int j=1;j<=rows-i;j++){

			System.out.print(" ");
		}
		
			for(int j=1;j<=2*i-1;j++){
			
				if(j<i){
				
					System.out.print(ch);
					ch++;
				}else{
				
					System.out.print(ch);
					ch--;
				}
				
			}
			System.out.println();
		
	}
	
	}
}
