
import java.io.*;

class program1{

	public static void main(String[] args)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.print("Enter number of rows: ");
	int rows=Integer.parseInt(br.readLine());

	System.out.println("Output :");
	int num=1;

	for(int i=1;i<=rows;i++){
	
		for(int j=1;j<=rows-i;j++){

			System.out.print(" ");
		}
		
			for(int k=1;k<=2*i-1;k++){
			
				System.out.print(num);
			}
			System.out.println();
		
	}
	
	}
}
