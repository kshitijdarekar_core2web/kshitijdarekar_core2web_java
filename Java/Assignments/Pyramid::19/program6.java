import java.util.*;

class program6{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows :");
		int rows=sc.nextInt();

		for(int i=1 ; i<=rows ; i++){
		
			int temp=rows; 

			for(int j=1 ; j<=rows-i ; j++){
			
				System.out.print(" ");
			}

			for(int j=1 ; j<=2*i-1 ; j++){
			
				if(j<i){
				
					System.out.print(temp);
					temp--;
				}else{
				
					System.out.print(temp);
					temp++;
				}	
			}

		System.out.println();
		}
		
	}
}
