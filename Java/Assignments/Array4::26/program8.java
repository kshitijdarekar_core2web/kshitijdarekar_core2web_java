import java.util.*;

class program8{

	public static void main(String[] args){
	
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of an array:");
		int size=sc.nextInt();
			
		char arr[]=new char[size];
		
		System.out.println("Enter elements in an array:");
		for(int i=0;i<size;i++){
		
			arr[i]=sc.next().charAt(0);
		}
		
		System.out.println("OUTPUT:");
		System.out.println("Enter Character that you want to search : ");
		int search=sc.next().charAt(0);

		int count=0;
		for(int i=0;i<size;i++){
			
			if(search==arr[i]){
			
				count++;
			}

		}
		System.out.println((char)search+" is occured "+count+" times in an array");
	}

}

