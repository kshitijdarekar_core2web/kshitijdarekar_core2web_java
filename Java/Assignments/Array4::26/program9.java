import java.util.*;

class program9{

	public static void main(String[] args){
	
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of an array:");
		int size=sc.nextInt();
			
		char arr[]=new char[size];
		
		System.out.println("Enter elements in an array:");
		for(int i=0;i<size;i++){
		
			arr[i]=sc.next().charAt(0);
		}
		
		System.out.println("OUTPUT:");
		for(int i=0;i<size;i++){
		
			if(arr[i]<'a' || arr[i]>'z'){
			
				arr[i]='#';
			}
		}
		
		for(int i=0;i<size;i++){

                        System.out.println(arr[i]);
                }


		}

}

