import java.util.*;

class program2{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of an array:");
		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.println("Enter elements in an array:");
		for(int i=0;i<size;i++){
		
			arr[i]=sc.nextInt();
		}
		
		int element=0;
		System.out.println("Output:");
		
		
		for(int i=0;i<size;i++){
		
			if(arr[i]>element){
			
				element=arr[i];
			}
		}
		int maximum=element;

		
		for(int i=0;i<size;i++){
		
			if(arr[i]<element){
			
				element=arr[i];
			}
		}
		int minimum=element;


		
		int difference=maximum-minimum;
		System.out.println("Difference between maximum and minimum is "+difference);
	}
}
