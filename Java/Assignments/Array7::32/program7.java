
import java.util.*;

class program7{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows size and columns size of an array");
		int arrRow=sc.nextInt();
		int arrCol=sc.nextInt();

		int arr[][]=new int[arrRow][arrCol];

		System.out.println("Enter array elements");
		for(int i=0;i<arrRow;i++){
	 	
	 		for(int j=0;j<arrCol;j++){
			
				arr[i][j]=sc.nextInt();
			}
		}


		 System.out.println("Output");


		 int product=1;

		 for(int i=0;i<arrRow;i++){

			 for(int j=0;j<arrCol;j++){

			 	if(i==j){
				
					product=product*arr[i][j];
				}
			 }
		 }
		 System.out.println("Product of digonal elements of array is "+product);


		 
	}
}
