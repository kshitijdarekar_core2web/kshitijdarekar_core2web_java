import java.util.*;

class program5{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows size and columns size of an array");
		int arrRow=sc.nextInt();
		int arrCol=sc.nextInt();

		int arr[][]=new int[arrRow][arrCol];

		System.out.println("Enter array elements");
		for(int i=0;i<arrRow;i++){
	 	
	 		for(int j=0;j<arrCol;j++){
			
				arr[i][j]=sc.nextInt();
			}
		}


		 System.out.println("Output");


		 for(int i=0;i<arrCol;i++){

			 int sum=0;	 
		
			 for(int j=0;j<arrRow;j++){   

				sum=sum+arr[j][i];	

			  }
		
		 System.out.println("Sum of column "+(i+1)+" of array is "+sum);
		 
		 }


		 
	}
}
