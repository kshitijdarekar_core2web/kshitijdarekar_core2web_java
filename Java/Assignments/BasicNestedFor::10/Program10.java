

class Program10{
	
	public static void main(String[] args){
	
		int row=4;

		for(int i=1;i<=row;i++){
			
			int num=i;
			
			for(int j=1;j<=row;j++){
			
				System.out.print(" "+num+" ");
				num++;
			}
			System.out.println();
		}
	}
}

//CHATGPT CODE:

/*
class Program10{

    	public static void main(String[] args) {
    
	    	int Rows = 3; // You can change this value for a different number of rows

        for (int i = 1; i <= Rows; i++) {
            
		for (int j = 0; j < Rows; j++) {
                
			System.out.print((i + j) + " ");
            }

            System.out.println();
        }
    }
}
*/
