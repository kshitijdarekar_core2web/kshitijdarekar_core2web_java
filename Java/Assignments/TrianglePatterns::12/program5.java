
import java.util.*;

class program5{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();
	
		

		for(int i=1;i<=row;i++){
		
			int chg=64+row;
			int chs=96+row;

			for(int j=1;j<=i;j++){
				
				if(i%2==0){
					
					System.out.print((char)chg+" ");
					chg--;
				}else{
				
					System.out.print((char)chs+" ");
					chs--;
				}
			
			}
			System.out.println();
		}
	}
}
