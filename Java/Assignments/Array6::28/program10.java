import java.util.*;

class program10{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		
		System.out.println("Enter array elements : ");
		for(int i=0;i<size;i++){
		
			arr[i]=sc.nextInt();
		}

		System.out.println("Output : ");

		Arrays.sort(arr);

		if(arr.length<3){
		
			System.out.println("Array size is less than 3");
		}else{
		
			System.out.println("Third largest elemet of array is "+arr[2]);
		}
	}
}
