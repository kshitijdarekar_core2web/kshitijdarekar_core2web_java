import java.util.*;

class program8{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter arrray size:");
		int size=sc.nextInt();

		char arr[]=new char[size]; 
		char newarr[]=new char[size]; 

		System.out.println("Enter array elements:");

		for(int i=0;i<size;i++){
			
			arr[i]=sc.next().charAt(0);

		}
		
		System.out.println("Output");

		System.out.println("Before Reverse:");

		for(int i=0;i<size;i++){
		
			System.out.print( arr[i]+" , ");
		}
		
		int temp=0;

		for(int i=size-1;i>=0;i--){
		
			newarr[temp]=arr[i];
			temp++;
		}

		System.out.println();
		System.out.println("After Reverse:");

		for(int i=0;i<size;i++){
		
			System.out.print( newarr[i]+" , ");
		}

	}
}
