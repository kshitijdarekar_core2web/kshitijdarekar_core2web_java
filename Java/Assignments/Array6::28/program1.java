import java.util.*;

class program1{

	public static void main(String[] args){

	Scanner sc=new Scanner(System.in);
	System.out.println("Enter size of an array");
	int size=sc.nextInt();

	int arr[]=new int[size];

	System.out.println("Enter array elements");
	for(int i=0;i<size;i++){
	
		arr[i]=sc.nextInt();
	}

	System.out.println("Output");

	int count=0;

	for(int i=0 ;i<size-1;i++){
	
		if(arr[i]>arr[i+1]){
		
			count++;
		}
	}

	if(count==size-1){
	
		System.out.println("Array is in descending order");
	}else{
	
		System.out.println("Array is not in descending order");
	}
	
	}
}
