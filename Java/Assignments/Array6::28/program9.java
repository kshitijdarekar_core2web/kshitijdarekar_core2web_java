import java.util.*;

class program9{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size=sc.nextInt();
		int arr[]=new int[size];
		int newarr[]=new int[size];
		
		System.out.println("Enter array elements : ");
		for(int i=0;i<size;i++){
		
			arr[i]=sc.nextInt();
		}

		System.out.println("Output : ");

		//to check palindrome

		for(int i=0;i<size;i++){
		
			int temp=arr[i];

			while(temp>0){

				newarr[i]=(newarr[i]*10)+(temp%10);
				temp=temp/10;	
			}
			

		}
		
		int count=0;
		for(int i=0;i<size;i++){
		
			if(arr[i]==newarr[i]){
			
				count++;
			}
		}

		System.out.print("Count of palindrome element is : "+count);
		System.out.println();		

	}
}
