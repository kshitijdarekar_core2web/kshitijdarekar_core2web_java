

import java.util.*;

class program6{

	public static void main(String[] args){

	Scanner sc=new Scanner(System.in);
	System.out.println("Enter size of an array:");
	int size1=sc.nextInt();

	int arr1[]=new int[size1];

	System.out.println("Enter array1 elements");

	for(int i=0;i<size1;i++){
	
		arr1[i]=sc.nextInt();
	}

	System.out.println("Output");
	
	System.out.println("Enter the number you want to find multiple of");

	int multiple=sc.nextInt();
	int count=0;

	for(int i=0;i<size1;i++){
	
		if(arr1[i]%multiple==0){
		
			System.out.println("An element multiple of "+multiple+" found at index : "+i);
		}
	
	}

	}
}
