

import java.util.*;

class program7{

	public static void main(String[] args){

	Scanner sc=new Scanner(System.in);
	System.out.println("Enter size of array");
	int size1=sc.nextInt();

	int arr1[]=new int[size1];

	System.out.println("Enter array1 elements");

	for(int i=0;i<size1;i++){
	
		arr1[i]=sc.nextInt();
	}

	System.out.println("Output");

	for(int i=0;i<size1;i++){
	
		if(arr1[i]>=65 && arr1[i]<=90){
			
			System.out.println((char)arr1[i]);
			
		}else{
			
			System.out.println(arr1[i]);
		}
	}
	
	}
}
