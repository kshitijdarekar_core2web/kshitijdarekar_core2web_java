import java.util.*;

class program8{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();

		
		System.out.println("Output:");


		for(int i=1;i<=row;i++){

			int temp=i;

			for(int j=1;j<i;j++){
			
				System.out.print(" ");
			}
				
				for(int k=1;k<=row-i+1;k++){
				
					System.out.print(temp);
					temp++;

				}
				System.out.println();
				
		}
			
	}
}

