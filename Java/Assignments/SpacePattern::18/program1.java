import java.util.*;

class program1{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();

		System.out.println("Output:");

		for(int i=1;i<=row;i++){
		
			int l=1;

			for(int j=1;j<=row-i;j++){
			
				System.out.print(" ");
			}
				
				for(int k=1;k<=i;k++){
				
					System.out.print(l);
					l++;
				}
				System.out.println();
		}
			
	}
}

