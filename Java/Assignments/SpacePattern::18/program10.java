import java.util.*;

class program10{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row=sc.nextInt();

		
		System.out.println("Output:");


		for(int i=1;i<=row;i++){

			int temp=65;

			for(int j=1;j<i;j++){
			
				System.out.print(" ");
				temp++;
			}
				
				for(int k=1;k<=row-i+1;k++){
				
					if(row%2==0){
						
						if((k+i)%2!=0){

							System.out.print((char)temp);

						}else{
					
							System.out.print(temp);

						}
						temp++;
					}else{
					
						if((k+i)%2==0){

						System.out.print((char)temp);

						}else{
					
						System.out.print(temp);

						}
						temp++;
					}
					
				
				}
				System.out.println();	
		}

	}
}
