import java.io.*;

class program4{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an array : ");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements : ");

		for(int i=0;i<size;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}
		
		System.out.println("Output : ");

		System.out.println("Enter the number to search in an array : ");
		int search=Integer.parseInt(br.readLine());

		for(int i=0;i<size;i++){
			
				if(arr[i]==search){

					System.out.println(arr[i]+" is at index "+i);

				}
			}
		
		}

	}

