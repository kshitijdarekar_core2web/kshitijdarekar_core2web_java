import java.io.*;

class program1{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of an array : ");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		int count=0;

		System.out.print("Enter array elements : ");

		for(int i=0;i<size;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
			
				count++;
			}
		}

		System.out.println("Count of even numbers is : "+count);

		System.out.print("Even numbers in an array :");
		
		for(int i=0;i<size;i++){
			
			if(arr[i]%2==0){
				
				System.out.print(arr[i]+" , ");
			}
		
		}

	}
}
