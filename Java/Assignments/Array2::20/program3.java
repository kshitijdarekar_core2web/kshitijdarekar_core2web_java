import java.io.*;

class program3{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of an array : ");
		int size=Integer.parseInt(br.readLine());

		char arr[]=new char[size];

		System.out.print("Enter array elements : ");

		for(int i=0;i<size;i++){
		
			arr[i]=(char)br.read();

			if(arr[i]=='\n'){
			
				i--;
			}

		}
		
		System.out.println("Vowels in an array are : ");

		for(int i=0;i<size;i++){
			
			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u' || arr[i]=='A' || arr[i]=='E' || arr[i]=='I' || arr[i]=='O' || arr[i]=='U'){
				
				System.out.println(arr[i]);
			}
		
		}

	}
}
