import java.io.*;

class program6{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an array : ");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements : ");

		for(int i=0;i<size;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}
		
		System.out.println("Output : ");
		int product1=1;

		for(int i=0;i<size;i++){
			
				if(i%2!=0){
				
					product1=product1*arr[i];
				}	
				
		}
		System.out.println("product1 of all odd elements in array is : "+product1);
	}
		
}

	

