import java.io.*;

class program9{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an array : ");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements : ");

		for(int i=0;i<size;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}
		
		int min=0;

		System.out.println("Output : ");

		for(int i=0;i<size;i++){
			
				if(arr[i]<min){
				
					min=arr[i];
				}
				
		}
		System.out.println("Minimum element of array is : "+min);

	}
		
}

	

