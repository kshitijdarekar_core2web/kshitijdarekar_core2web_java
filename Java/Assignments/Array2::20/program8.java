import java.io.*;

class program8{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an array : ");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements : ");

		for(int i=0;i<size;i++){
		
			arr[i]=Integer.parseInt(br.readLine());

		}
		
		System.out.println("Output : ");

		for(int i=0;i<size;i++){
			
				if(arr[i]<9 && arr[i]>5){

					System.out.println(arr[i]+" is greater than 5 and less then 9");	
				}
				
		}

	}
		
}

	

