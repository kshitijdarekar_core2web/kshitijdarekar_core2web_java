
import java.io.*;
class n9{
	public static void main(String[] args)throws IOException{
		BufferedReader ip = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number = ");
		int num = Integer.parseInt(ip.readLine());
		int var = 0;
		int temp = num;
		while(num>0){
			int rem = num%10;
			var = var*10+rem;
			num/=10;
		}
		if(temp == var ){
			System.out.println("Palindrome");
		}else{
			System.out.println("Given number is not palindrome");
		}
	}
}
