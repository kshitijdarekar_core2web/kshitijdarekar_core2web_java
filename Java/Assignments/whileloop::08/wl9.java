
class wl9{
	public static void main(String[] args){
		int num = 214367689;
		int digit = 0;
		int oddcount = 0;
		int evencount = 0;
		while(num>0){
			
			 digit = num%10;
			 
			if(digit%2!=0){
				oddcount++;
			
			}else{
				evencount++;
			}
			num = num/10;

		}
		System.out.println("Number of odd digits are " + oddcount);
		System.out.println("Number of even digits are " + evencount);
	}
}
