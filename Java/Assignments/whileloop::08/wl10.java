class wl10{
	public static void main(String[] args){
		int num = 930792240;
		int sum = 0;
		while(num>0){
			int digit = num%10;
			sum = sum+digit;
			num = num/10;
		}

		System.out.println("Sum of digits in a number is "+sum);
	}
	
}
