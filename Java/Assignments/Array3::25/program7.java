
import java.util.*;

class program7{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array :");
		int size=sc.nextInt();
		
		int arr[]=new int[size];

		System.out.println("Enter elements in array : ");

		for(int i=0 ; i<size ; i++){
		
			arr[i]=sc.nextInt();
		}
	
		System.out.println("Output : ");

		if(size>=5 && size%2!=0){

			for(int i=0 ; i<size ; i++){		
	
			if(i%2==1){
				
				System.out.println(arr[i]);
			}
			}
		}else{
		
			for(int i=0 ; i<size ; i++){

                        if(i%2==0){

                                System.out.println(arr[i]);
			}
			}
		}
		
	
	
}
		
	
}
