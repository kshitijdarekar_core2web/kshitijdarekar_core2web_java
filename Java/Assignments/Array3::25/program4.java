
import java.util.*;

class program4{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array :");
		int size=sc.nextInt();
		
		int arr[]=new int[size];

		System.out.println("Enter elements in array : ");

		for(int i=0 ; i<size ; i++){
		
			arr[i]=sc.nextInt();
		}
	
		System.out.println("Output : ");


		for(int i=0 ; i<size ; i++){		
	
			if(i%2==0){
				
				arr[i]=1;
			}else{
			
				arr[i]=0;
			}
		}

		for(int i=0 ; i<size ; i++){
			
			System.out.print(arr[i]+" , ");
		}
		
	
	}
}
