
import java.util.*;

class program3{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array :");
		int size=sc.nextInt();
		
		int arr[]=new int[size];

		System.out.println("Enter elements in array : ");

		for(int i=0 ; i<size ; i++){
		
			arr[i]=sc.nextInt();
		}
		

		System.out.println("Enter number which you want to search : ");
		int search=sc.nextInt();

		System.out.println("Output : ");
		int count=0;

		for(int i=0 ; i<size ; i++){
		
			if(search==arr[i]){
				count++;
			}
		}

		System.out.println(search+" occured "+count+" times in an array");
		
	
	}
}
