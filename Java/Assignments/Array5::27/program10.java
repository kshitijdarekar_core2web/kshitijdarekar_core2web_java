import java.util.*;

class program10{

    public static void main(String[] args){

        Scanner sc=new Scanner(System.in);

        System.out.println("Enter element : ");

        int element=sc.nextInt();

        System.out.println("Output");

        int count=0;
        int temp;

        while(element!=0){
            temp=element%10;
            count=count*10+temp;
            element=element/10;
        }

        while (count != 0) {

            int digit = count % 10;

            int factorial = 1;

            
            for(int i=1; i<=digit; i++) {
                factorial =factorial * i;
            }
            System.out.println("Factorial of " + digit + " is: " + factorial);
            count = count / 10;
        }
    }
}

