
import java.util.*;

class program2{                                                                                                                                         
    
	public static void main(String[] args){	
        
		Scanner sc=new Scanner(System.in);
        
		System.out.println("Enter size of an array");
		int size=sc.nextInt();
		int arr1[]=new int[size];

        	System.out.println("Enter elements in an array");

		for(int i=0;i<size;i++){

                	arr1[i]=sc.nextInt(); 
		}

		System.out.println("Output");

        	int EvenSum=0;
		int OddSum=0;

		for(int i=0;i<size;i++){


			if(arr1[i]%2==0){
			
				EvenSum=EvenSum+arr1[i];

			}else{
		
				OddSum=OddSum+arr1[i];
			}
			
		}


             	System.out.println("Sum of even numbers is "+EvenSum);

      	 
		System.out.println("Sum of odd numbers is "+OddSum); 
	}
}

