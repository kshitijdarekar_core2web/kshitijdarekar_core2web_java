import java.util.*;

class program5{

    public static void main(String[] args){

        Scanner sc=new Scanner(System.in);

        System.out.println("Enter size of an array");
        int size=sc.nextInt();
        int arr[]=new int[size];

        System.out.println("Enter elements in an array");

        for(int i=0;i<size;i++){
            arr[i]=sc.nextInt();
        }

        System.out.println("Output");

        for(int i=0 ;i<size ;i++){
	
		int count=0;
		
		if(arr[i]!=0){
			
			while(arr[i]!=0){
		
				arr[i]=arr[i]/10;
				count++;
			}
		}else{
		
			count=1;
		}

		System.out.print(count+" , ");
	}
    }
}

