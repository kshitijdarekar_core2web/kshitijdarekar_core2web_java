import java.util.*;

class program4{

    public static void main(String[] args){

        Scanner sc=new Scanner(System.in);

        System.out.println("Enter size of an array");
        int size=sc.nextInt();
        int arr[]=new int[size];

        System.out.println("Enter elements in an array");

        for(int i=0;i<size;i++){
            arr[i]=sc.nextInt();
        }

        System.out.println("Output");

        int count=0;
        int index=-1;

        for(int i=0 ; i<size ; i++){
            
		count = 0;
            
		for(int j=i+1 ; j<size ; j++){

                	if(arr[i]==arr[j]){
                    		
				count++;
				index=i;
				break;
                	}
            }
            if(count>0){
                break;
            }
        }

        if(index != -1){
            System.out.println("First duplicate element at index "+index);
        } else {
            System.out.println("No duplicate elements found.");
        }
    }
}

