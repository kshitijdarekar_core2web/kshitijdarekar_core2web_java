import java.util.*;

class program6{

    public static void main(String[] args){

        Scanner sc=new Scanner(System.in);

        System.out.println("Enter size of an array");
        int size=sc.nextInt();
        int arr[]=new int[size];

        System.out.println("Enter elements in an array");

        for(int i=0;i<size;i++){
            arr[i]=sc.nextInt();
        }

        System.out.println("Output");

	int index=0;
	int count=0;

        for(int i=0 ;i<size ;i++){
	
		count=0;
		
		for(int j=1 ;j<=arr[i]/2 ;j++){
		
			if(arr[i]%j==0){
			
				count++;
			}
			
		}
	
		if(count==1){

			index=i;
			break;
		}
	}

	if(count==1){
	
		System.out.println("First prime number is at index "+index);
	}else{
	
		System.out.println("There is no prime number in array");
	}

    }
}

