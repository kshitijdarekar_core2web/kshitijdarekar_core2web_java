class Bit1{
	public static void main(String[] args){
		int x = 10;
		int y = 12;

		System.out.println(x&y);	// 0000 1010	
		System.out.println(x|y);	// 0000 1100	
	}
}
		// 0000 1010 	0 0 = 0
		// 0000 1100	0 1 = 0
	/// 	   0000 1000   	1 0 = 0		= this is for only &.
	//			1 1 = 1		
//
//
		// 0000 1010	0 0 = 0
		// 0000 1100	0 1 = 1
	/// 	   0000 1110	1 0 = 1		= this is for only |.
//				1 1 = 1	     	
