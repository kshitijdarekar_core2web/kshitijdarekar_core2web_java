class ThreadGroupDemo extends Thread{

	ThreadGroupDemo(ThreadGroup tg , String str){
	
		super(tg,str);
	}
	
	public void run(){

		System.out.println(Thread.currentThread());
		try{
		
			Thread.sleep(2000); //when we do get active count threads are becoming dead before getting count hence timer is added 
		}catch(Exception e){
		

		}
	}

}
class Client{

	public static void main(String[] args){
	
		System.out.println(Thread.currentThread().getName());

		ThreadGroup Tg=new ThreadGroup("Instagram");
		ThreadGroup SubTg1=new ThreadGroup(Tg,"Reels");
		ThreadGroup SubTg2=new ThreadGroup(Tg,"Posts");

		ThreadGroupDemo t1=new ThreadGroupDemo(SubTg1,"Kshitij");
		ThreadGroupDemo t2=new ThreadGroupDemo(SubTg1,"Prasad");
		ThreadGroupDemo t3=new ThreadGroupDemo(SubTg2,"Shashwat");
		ThreadGroupDemo t4=new ThreadGroupDemo(SubTg2,"Harshal");

		t1.start();
		t2.start();
		t3.start();
		t4.start();

		System.out.println(Tg.getParent().getName()); //Methods
		System.out.println(Tg.activeCount());
		System.out.println(SubTg2.activeCount());
		System.out.println(Tg.activeGroupCount());
		System.out.println(SubTg1.getParent().getName());
		

		
	}
}
