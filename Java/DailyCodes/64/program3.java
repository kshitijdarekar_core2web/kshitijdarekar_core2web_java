class Demo{

	void fun(){
		
		System.out.println("In fun");
	}
	public void finalize(){ //Known as Destructor
	
		System.out.println("In finalize");
	}
}
class Client{

	public static void main(String[] args)throws InterruptedException{
	
		System.out.println("Start Main");

		Demo obj=new Demo();
		obj.fun();

		obj=null; //Making it null explicitly because it will only become null if reference is lost otherwise

		System.gc();

		Thread.sleep(2000);

		System.out.println("End Main");

	}
}
