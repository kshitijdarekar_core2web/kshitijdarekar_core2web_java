import java.util.concurrent.*;

class TaskDemo2 implements Runnable{

	int num=0;
	
	TaskDemo2(int num){
	
		this.num=num;
	}

	public void run(){
	
		System.out.println(Thread.currentThread().getName()+"Started on task"+num);
		fun();
		System.out.println(Thread.currentThread().getName()+"Ended on task"+num);

	}

	void fun(){
	
		try{
			Thread.sleep(2000);

		}catch(Exception e){
		
		}
	}
}
class Client2{

	public static void main(String[] args){
	
		
		ExecutorService threadpool=Executors.newSingleThreadExecutor();

		for(int i=0;i<=10;i++){
		
			TaskDemo2 t1=new TaskDemo2(i);
			threadpool.execute(t1);
			
		}
	}
}
