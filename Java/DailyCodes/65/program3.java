import java.util.concurrent.*;

class TaskDemo3 implements Runnable{

	int num=0;
	
	TaskDemo3(int num){
	
		this.num=num;
	}

	public void run(){
	
		System.out.println(Thread.currentThread().getName()+"Started on task"+num);
		fun();
		System.out.println(Thread.currentThread().getName()+"Ended on task"+num);

	}

	void fun(){
	
		try{
			Thread.sleep(2000);

		}catch(Exception e){
		
		}
	}
}
class Client3{

	public static void main(String[] args){
	
		
		ExecutorService threadpool=Executors.newCachedThreadPool();

		for(int i=0;i<=10;i++){
		
			TaskDemo3 t1=new TaskDemo3(i);
			threadpool.execute(t1);
			
		}
	}
}
