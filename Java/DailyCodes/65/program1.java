import java.util.concurrent.*;

class TaskDemo implements Runnable{

	int num=0;
	
	TaskDemo(int num){
	
		this.num=num;
	}

	public void run(){
	
		System.out.println(Thread.currentThread().getName()+"Started on task"+num);
		fun();
		System.out.println(Thread.currentThread().getName()+"Ended on task"+num);

	}

	void fun(){
	
		try{
			Thread.sleep(2000);

		}catch(Exception e){
		
		}
	}
}
class Client{

	public static void main(String[] args){
	
		
		ExecutorService threadpool=Executors.newFixedThreadPool(10);

		for(int i=0;i<=10;i++){
		
			TaskDemo t1=new TaskDemo(i);
			threadpool.execute(t1);
			
		}
	}
}
