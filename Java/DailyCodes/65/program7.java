class ThreadDemo3 extends Thread{

	ThreadDemo3(Runnable r){
	
		super(r);
	}

	public static void main(String[] args)throws InterruptedException{
	
		
		Runnable ref = ()-> {

				System.out.println(Thread.currentThread().getName());
		};
		

		ThreadDemo3 t1=new ThreadDemo3(ref);
		t1.start();

		System.out.println(Thread.currentThread().getName());

	}
}
