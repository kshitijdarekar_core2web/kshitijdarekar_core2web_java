import java.util.concurrent.*;

class TaskDemo4 implements Runnable{

	int num=0;
	
	TaskDemo4(int num){
	
		this.num=num;
	}

	public void run(){
	
		System.out.println(Thread.currentThread().getName()+"Started on task"+num);
		fun();
		System.out.println(Thread.currentThread().getName()+"Ended on task"+num);

	}

	void fun(){
	
		try{
			Thread.sleep(2000);

		}catch(Exception e){
		
		}
	}
}
class Client4{

	public static void main(String[] args){
	
		
		ThreadPoolExecutor threadpool1=(ThreadPoolExecutor)Executors.newCachedThreadPool();

		for(int i=0;i<=10;i++){
		
			TaskDemo4 t1=new TaskDemo4(i);
			threadpool1.execute(t1);
			
		}


		ThreadPoolExecutor threadpool2=(ThreadPoolExecutor)Executors.newFixedThreadPool(10);

		for(int i=0;i<=10;i++){
		
			TaskDemo4 t1=new TaskDemo4(i);
			threadpool2.execute(t1);
			
		}
		threadpool1.shutdown();
		threadpool2.shutdown();
	}

	
}
