class program11{

	public static void main(String[] args){
	
		//way 1
		String str1="Shashi"; //SCP
		System.out.println(str1);

		//way 2
		String str2="Shashi";  //SCP
		System.out.println(str2);

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
	}
}
