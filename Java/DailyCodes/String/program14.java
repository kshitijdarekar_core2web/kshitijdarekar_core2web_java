class program14{

	public static void main(String[] args){
	
	
		String str1="Kshitij"; //SCP i.e String Literal
		System.out.println(System.identityHashCode(str1));

		
		String str2="Kshitij Darekar";  //SCP i.e String Literal
		System.out.println(System.identityHashCode(str2));
		

		String str3=new String("Kshitij");  //Heap i.e object type string or new string
		System.out.println(System.identityHashCode(str3));
		
		
		String str4=new String("Kshitij");  //Heap i.e object type string or new string
		System.out.println(System.identityHashCode(str4));
		
	}
}
