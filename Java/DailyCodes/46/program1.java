class SEBI{

	SEBI(){
	
		System.out.println("securities deposited by the client shall be kept in a separate account");
	}
}

class Zerodha extends SEBI{

	Zerodha(){
	
		System.out.println("When selling holdings, 80% of the selling credit can be used for new trades");
	}

	void Futures(){
	
		System.out.println("Flat ₹ 20 or 0.03% (whichever is lower) per executed order");
	}
}

class Stocks{

	public static void main(String[] args){
	
		Zerodha obj=new Zerodha();
		obj.Futures();
	
	}

}
