import java.util.*;

class program1{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int row=sc.nextInt();

		
		System.out.println("Pattern :");
		
		int temp=1;
		for(int i=0;i<row;i++){
		
			for(int j=row-i;j>=0;j--){
			
				System.out.print("\t");
			}

			for(int j=0;j<=i;j++){
				

				System.out.print(temp+"\t");
				temp+=2;
			}
			System.out.println();


		}
	}
}
