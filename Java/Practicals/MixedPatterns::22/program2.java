import java.util.*;

class program2{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int row=sc.nextInt();

		
		System.out.println("Pattern :");
		
		int temp=1;
		for(int i=1;i<=row;i++){
		
			for(int j=1;j<i;j++){
			
				System.out.print("\t");
			}

			for(int j=1;j<=row-i+1;j++){
				

				System.out.print(temp+"\t");
				temp++;
			}

			temp=temp-1;
			System.out.println();


		}
	}
}
