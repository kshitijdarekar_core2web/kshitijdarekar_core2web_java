import java.util.*;

class program4{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int row=sc.nextInt();

		
		System.out.println("Pattern :");
		

		for(int i=1;i<=row;i++){

			int small=97;
                	int cap=65;
		
			for(int j=1;j<i;j++){
			
				System.out.print("\t");
				cap++;
				small++;
			}

			for(int j=1;j<=row-i+1;j++){
				
				if(row%2==0){
				
					System.out.print((char)small+"\t");
					small++;
				}else{
				
					System.out.print((char)cap+"\t");
					cap++;
				}
			}
			System.out.println();


		}
	}
}
