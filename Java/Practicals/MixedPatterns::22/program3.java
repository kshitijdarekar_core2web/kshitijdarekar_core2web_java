import java.util.*;

class program3{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int row=sc.nextInt();

		
		System.out.println("Pattern :");
		
		int num=1;
		int temp=row;
		for(int i=1;i<=row;i++){
		
			for(int j=1;j<=row-i;j++){
			
				System.out.print("\t");
			}

			for(int j=1;j<=i;j++){
				

				System.out.print(num+"\t");
				num=num+temp;
			}
			System.out.println();


		}
	}
}
