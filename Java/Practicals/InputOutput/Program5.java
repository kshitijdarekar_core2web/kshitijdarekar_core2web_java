import java.util.*;

class program5{

	public static void main(String[] args){
	
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter any number: ");
		int num=sc.nextInt();

		if(num>=16){
		
			if(num%16==0){
			
				System.out.println(num+" is divisible by 16");
			}else{
			
				System.out.println(num+" is not divisible by 16");
			}
		}else{
		
			 System.out.println(num+" entered is invalid");
		}
	}
}
