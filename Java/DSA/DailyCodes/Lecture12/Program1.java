/*
Qn. Given an matrix of size N*M print all the diagonals (R-->L).Digonal starting from 0th row

Row=5
Column=6
int arr[][]=new int[][]{
{1,2,3,4,5,6},
{7,8,9,10,11,12},
{13,14,15,16,17,18},
{19,20,21,22,23,24},
{25,26,27,28,29,30},
}
*/

class digonals{
	
	public static void main(String[] args){
	
	int Row=5;
	int Column=6;

	int arr[][]=new int[][]{                                                                                                                                   	    {1,2,3,4,5,6},
		{7,8,9,10,11,12},
		{13,14,15,16,17,18},
		{19,20,21,22,23,24},
		{25,26,27,28,29,30},
	};

	for(int i=0;i<5;i++){
	
		int k=i;

		for(int j=5;j>=0;j--,k++){
		
			if(k>4 || j<0){
			
				break;
			}
			System.out.println(arr[k][j]);
		}
	}
	
	}
}
