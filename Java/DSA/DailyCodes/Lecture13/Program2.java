import java.util.*;

class MaxSubSumOfLenN{

	public static void main(String[] args){
	
		int arr[]=new int[]{-3,4,-2,5,3,-2,8,2,1,4};

		System.out.print("Enter length of subarray :");
		Scanner sc=new Scanner(System.in);
		int k=sc.nextInt();
		int start=0;
		int end=k-1;
		int sum=Integer.MIN_VALUE;
		
		while(end<arr.length){

			int temp=0;

			for(int i=start ; i<=end ;i++){
			
				temp=temp+arr[i];

				if(temp>sum){
                                	sum=temp;
                       		 }
			}
			start++;
			end++;
		}
		System.out.println("Maximum Sum of Subarray of length N is : "+sum);

		}
		
	}

