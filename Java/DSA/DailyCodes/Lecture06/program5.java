class Program5 {

    public static void main(String[] args) {
        int arr[] = new int[]{1, 2, 3, 4, 5, 6};
        int max = Integer.MIN_VALUE;
        int secmax = Integer.MIN_VALUE;

        // Find the maximum value in the array
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.println("Maximum value: " + max);

        // Find the second maximum value in the array
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > secmax && arr[i] < max) {
                secmax = arr[i];
            }
        }
        System.out.println("Second maximum value: " + secmax);
    }
}

