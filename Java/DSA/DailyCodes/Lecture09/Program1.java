class program1{

	public static void main(String[] args){
	
		int Arr[]=new int[]{-7,1,5,2,-4,3,0};
		int PsArr[]=new int[Arr.length];

		int count=0;
		int EqIndex=Integer.MAX_VALUE;

		PsArr[0]=Arr[0];
		for(int i=1;i<Arr.length;i++){

			PsArr[i]=PsArr[i-1]+PsArr[i];
		}


		for(int i=0;i<Arr.length;i++){
			
			int leftsum=0;
			int rightsum=0;

			for(int j=0;j<i;j++){
			
				leftsum=leftsum+Arr[j];
			}

			for(int k=i+1;k<Arr.length;k++){
			
				rightsum=rightsum+Arr[k];
			}

			if(leftsum==rightsum){
			
				count++;
				if(i<EqIndex){
				
					EqIndex=i;
				}
			}

		}
		if(count==0){

			System.out.println("-1");
		}else{
			
			System.out.println(count);	
			System.out.println(EqIndex);
		}
	
	}
}
