class Fibo{

	int fibo(int num){
	
		if(num<1){
		
			return num;
		}
		return fibo(num-1)+fibo(num-3);
	}

	public static void main(String[] args){
	
		int num=4;
		Fibo obj=new Fibo();
		int ret=obj.fibo(4);
		System.out.println(ret);
	}
}
