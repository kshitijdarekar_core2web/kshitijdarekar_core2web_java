//BRUTFORCE

class Demo{

	public static void main(String[] args){
	
		int cnt=0;
		int iteration=0;
		char arr[]=new char[]{'a','b','e','g','a','g'};

		for(int i=0 ; i<arr.length ; i++){
		
			if(arr[i]=='a'){
				
				for(int j=i+1 ; j<arr.length ; j++){
				
					if(arr[j]=='g'){
					
						cnt++;
					}
					iteration++;
				}

			}
		
		}
		System.out.println(cnt);
		System.out.println("No of iterations are "+iteration);
	}
}

//OPTIMIZED 


//[a,g,a,g,a,g]
//[g,a,a,a,c,g,g,g]
//This is optimize approach
class Demo22{

	public static void main(String[] args){
	
		int cntA=0;
		int pair=0;
		char arr[]=new char[]{'a','b','e','g','a','g'};
		
		for(int i=0;i<arr.length;i++){
		
			if(arr[i]=='a'){
				
				cntA++;

			}else if(arr[i]=='b'){
			
				pair=pair+cntA;

			}

		}
		System.out.println(pair);

	}
}
