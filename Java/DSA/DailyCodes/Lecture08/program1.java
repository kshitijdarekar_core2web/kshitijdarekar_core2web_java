import java.util.*;

class kd{

	static int maxEle(int index){
	
		int N=7;
		int Max=Integer.MIN_VALUE;
		int arr[]=new int[]{3,4,5,1,2,7,9,8};

		for(int i=0;i<=index;i++){
			
			if(arr[i]>Max)
				Max=arr[i];
		}
		return Max;
	
	}

	public static void main(String[] args){
	
		int index=3;
		int ret=maxEle(index);
		System.out.println("Maximum element till index "+index+" is "+ret);
	}
}
