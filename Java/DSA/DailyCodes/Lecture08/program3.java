//Optimized Approach Right Max Array

class RightMaxArr{

	public static void main(String[] args){
	
		
                int N=10;
                int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};

                int RightMaxArr[]=new int[N];
                RightMaxArr[N-1]=arr[N-1];

		for(int i=N-2 ; i>=0 ; i--){
		
			if(RightMaxArr[i+1]<arr[i]){
			
				RightMaxArr[i]=arr[i];

			}else{
			
				RightMaxArr[i]=RightMaxArr[i+1];
			}
		}

		for(int i=0;i<N;i++){
		
			System.out.print(RightMaxArr[i]+",");
		}

	}
}
