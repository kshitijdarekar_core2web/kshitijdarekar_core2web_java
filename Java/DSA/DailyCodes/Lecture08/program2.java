/*

//BRUTFORCE

class LeftMaxArr{

	public static void main(String[] args){

		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int N=10;
		int LeftMaxArr[]=new int[N];
		int Max=Integer.MIN_VALUE;

		for(int i=0 ; i<N ; i++){
		

			for(int j=0; j<=i ; j++){

				if(arr[i]>Max){
				
					Max=arr[i];
				}
			}

			LeftMaxArr[i]=Max;
		}

		for(int i=0;i<N;i++){
		
			System.out.print(LeftMaxArr[i]+",");
		}	

	}
}

*/

//Optimized Approach

class LeftMaxArr{

	public static void main(String[] args){
	
		int N=10;
		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
		
		int LeftMaxArr[]=new int[N];
		LeftMaxArr[0]=arr[0];

		for(int i=1 ; i<N ; i++){
		
			if(LeftMaxArr[i-1]<arr[i]){
			
				LeftMaxArr[i]=arr[i];
			}else{
			
				LeftMaxArr[i]=LeftMaxArr[i-1];
			}
		}

		for(int i=0;i<N;i++){
		
			System.out.print(LeftMaxArr[i]+",");
		}
	}
}
