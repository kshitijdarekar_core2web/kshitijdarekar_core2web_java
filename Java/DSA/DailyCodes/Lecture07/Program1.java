import java.util.*;

class ArraySum{

	public static void main(String[] args){
	
		int arr[]=new int[]{2,5,3,11,7,9,4};
		int N=7;
		Scanner sc=new Scanner(System.in);
		int startindex=sc.nextInt();
		int endindex=sc.nextInt();
		int sum=0;

		if(startindex<0 || endindex>arr.length){
		
			System.out.println("Give Valid Input");
		}else{
		
			for(int i=startindex ; i<=endindex ; i++){
			
				sum=sum+arr[i];
			}
			System.out.println(sum);
		}	
	}
}
