import java.util.*;

class program2{

	public static void main(String[] args){
	
		int arr[]=new int[]{1,2,3,4,5};
		int minEle = Integer.MAX_VALUE;
		int maxEle = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
	
			if(arr[i] >= maxEle){
			
				maxEle=arr[i];
			
			}
			if(arr[i] <= minEle){
				
				minEle=arr[i];
			}
		}
		System.out.println("Maximum element is "+maxEle);
		System.out.println("Minimum element is "+minEle);

	}
}
