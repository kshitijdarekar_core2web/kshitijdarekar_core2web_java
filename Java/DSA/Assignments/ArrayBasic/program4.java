import java.util.*;

class program4{

	public static void main(String[] args){
	
		int arr[]=new int[]{1,2,3,4};
		int prod=1;

		for(int i=0;i<arr.length;i++){
	
			prod=prod*arr[i];
		}
		System.out.println("Product of all elements in array is "+prod);

	}
}
