import java.util.*;

class program3{

	public static void main(String[] args){
	
		int arr[]=new int[]{1,2,0,3,2,4,5};
		int maxEle = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
	
			if(arr[i] >= maxEle){
			
				maxEle=arr[i];
			
			}
		}
		System.out.println("Maximum element in an array is "+maxEle);

	}
}
